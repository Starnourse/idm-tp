# VideoGen

## Auteurs

* Cyril ALVES
* Jérémy THO

## Description

Ce projet est un générateur de vidéos développé lors du cours d'IDM (Ingénierie Des Modèles) du Master 2 Miage de Rennes avec M. Acher.

Ce-dit projet est à rendre au plus tard pour le 25/02/2018. Nous avons fait les 4 Tps, nous n'avons pas géré les images et n'avons pas pu faire le service web.

## Utilisation

* Mettez ffmpeg en variable système

* Lancez JUnit sur VideoGenToolSuite/src/VideoGenTest1.xtend plusieurs fois.

*	* Nous n'avons pas compris pourquoi la vidéo ne se lançait pas toujours dès le premier lancement JUnit. Nous avons essayé de creuser du côté des waitFor() sans trop de résultats.