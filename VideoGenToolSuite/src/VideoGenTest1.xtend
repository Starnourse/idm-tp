import org.junit.Test
import org.eclipse.emf.common.util.URI

import static org.junit.Assert.*
import org.xtext.example.mydsl.videoGen.MandatoryVideoSeq
import java.util.List
import java.util.ArrayList
import org.xtext.example.mydsl.videoGen.AlternativeVideoSeq
import org.xtext.example.mydsl.videoGen.OptionalVideoSeq
import org.xtext.example.mydsl.videoGen.VideoGeneratorModel
import java.io.PrintWriter
import java.io.File
import java.io.FileReader
import java.util.LinkedList
import org.xtext.example.mydsl.videoGen.VideoDescription
import java.io.FileWriter
import java.util.Random
import java.io.LineNumberReader
import java.io.BufferedReader
import org.xtext.example.mydsl.videoGen.Filter
import org.xtext.example.mydsl.videoGen.BlackWhiteFilter
import org.xtext.example.mydsl.videoGen.NegateFilter
import org.xtext.example.mydsl.videoGen.FlipFilter

class VideoGenTest1 {
	var res = 0
	var alternatives = 0
	
	Object random = new Random()

	@Test
	/**
	 * TP 2 - Question 1 et 2 
	 * Concat�ne la liste de vid�o
	 * 
	 * PS : LANCER PLUSIEURS FOIS LORS DU DE LA PREMIERE GENERATION
	 * Les vid�os doivent �tre g�n�r�es une premi�re fois pour pouvoir �tre concat�n�es /!\
	 * car le waitFor ne fonctionne pas avec..
	 */
	def void testLoadModel() {
		val nomVideogen = "example1"; // CHANGER POUR TESTER LES AUTRES EXEMPLES
		val nomMp4 = "out1.mp4"
		val nomGif = "out1.gif";
		
		
		val videoGen = new VideoGenHelper().loadVideoGenerator(URI.createURI(nomVideogen + ".videogen"))
		assertNotNull(videoGen)
		println(videoGen.information.authorName)		
		// and then visit the model
		// eg access video sequences: videoGen.videoseqs
		val file = new FileWriter("playlistFile.txt");
		videoGen.medias.forEach [video |
			
			if(video instanceof MandatoryVideoSeq){
				val desc = video.description
				var filtre = findFilter(desc.filter)
				var sousTitre = desc.text.content
				
				println("Adding mandatory video ..." + desc.videoid)
				println(desc.location)

				if (desc.text.content == null ){sousTitre = ""}
				
				if (filtre == "" && sousTitre == ""){
					file.write("file '"+desc.location + "' \n")
				}
				else {
					//supprimerFichier("modify_"+desc.location)
					file.write("file 'modify_"+desc.location + "' \n")
					applyFilterSubtitle(filtre, desc.text.content, desc.location)
				}
				
			}
			// Probabilit� r�partie entre les vid�os
			if(video instanceof AlternativeVideoSeq){
				println("Adding alternative video ...")
				val random = new Random
				// TODO: Prendre en compte les probabilit�s de chaque video
				val desc = video.videodescs.get(random.nextInt(video.videodescs.size))
				var filtre = findFilter(desc.filter)
				var sousTitre = desc.text.content
				
				if (desc.text.content == null ){sousTitre = ""}
				
				if (filtre == "" && sousTitre == ""){
					file.write("file '"+desc.location + "' \n")
					
				}
				else {
					file.write("file 'modify_"+desc.location + "' \n")
					applyFilterSubtitle(filtre , sousTitre, desc.location)
					
				}
				
			}
			// Probabilit� respect� par le model
			if(video instanceof OptionalVideoSeq){
				val random = new Random
				val desc = video.description
				var filtre = findFilter(desc.filter)
				var sousTitre = desc.text.content
				var probability = desc.probability
				
				println("Adding optional video ..." + desc.videoid)
				println(desc.location)
				
				if (desc.text.content == null){sousTitre = ""}
				if (desc.probability == "" ){probability = "50"}
				
				if (filtre == "" && sousTitre == ""){
					if (random.nextDouble() <= Double.parseDouble(probability)/100){
						file.write("file '"+desc.location + "' \n")
					}
				}
				else {
					if(random.nextDouble() <= Double.parseDouble(probability)/100){
						file.write("file 'modify_"+desc.location + "' \n")
						applyFilterSubtitle(filtre , sousTitre, desc.location)
					}
				}
			}
		]
		file.close()
		
		println("ffmpeg -y -f concat -safe 0 -i playlistFile.txt -c copy " + nomMp4)
		var p = Runtime.runtime.exec("ffmpeg -y -f concat -safe 0 -i playlistFile.txt -c copy " + nomMp4)
		p.waitFor
		
		Runtime.runtime.exec("ffplay -autoexit " + nomMp4)
	 	
	 	println("Cr�ation de la videoGen")
		
		createCSV(nomVideogen);
		
		testNbVariante(nomVideogen);
		
		generateGif(nomMp4, nomGif);
		
		addRealSize(nomMp4);	
	}
	
	def String findFilter(Filter filter) {
		
		if(filter instanceof BlackWhiteFilter){
			return "b&w"
		}
		else if(filter instanceof NegateFilter){
			return "negate"
		}
		else if(filter instanceof FlipFilter){
			return filter.orientation
		}
		return ""
	}
	
	/**
	 * Applique un filtre et un sous-titre � la vid�o
	 * TP4 - Q7
	 */
	 def void applyFilterSubtitle(String filtre, String drawtext, String videoName){
	 	println("Modify video : "+filtre+" / "+drawtext+" / "+videoName)
	 	if (filtre == "negate"){
	 		println("== negate ==")
			var p = Runtime.runtime.exec("ffmpeg -y -i " + videoName + " -vf lutrgb=\"r=negval:g=negval:b=negval, drawtext=fontfile=FreeSerif.ttf: text='"+ drawtext +"'\" modify_" + videoName)
			//p.waitFor
	 	}
	 	else if(filtre == "b&w"){
	 		println("== b&w == ")
	 		var p = Runtime.runtime.exec("ffmpeg -y -i " + videoName + " -vf \"hue=s=0, drawtext=fontfile=FreeSerif.ttf: text='"+ drawtext +"'\" modify_" + videoName)
	 		//p.waitFor
		}
	 	else if(filtre == "v" || filtre == "vertical"){
	 		println("== v == ")
	 		var p = Runtime.runtime.exec("ffmpeg -y -i " + videoName + " -vf \"transpose=1, drawtext=fontfile=FreeSerif.ttf: text='"+ drawtext +"'\" modify_" + videoName)
	 		//p.waitFor
	 	}
	 	else if(filtre == "h" || filtre == "horizontal"){
	 		println("== h ==")
	 		var p = Runtime.runtime.exec("ffmpeg -y -i " + videoName + " -vf \"transpose=2,transpose=2, drawtext=fontfile=FreeSerif.ttf: text='"+ drawtext +"'\" modify_" + videoName)
	 		//p.waitFor
	 	}
	 	else if (filtre == "" && drawtext != ""){
	 		println("== ?? ==")
	 		var p = Runtime.runtime.exec("ffmpeg -y -i " + videoName + " -vf \"drawtext=fontfile=FreeSerif.ttf: text='"+ drawtext +"'\" modify_" + videoName)
	 		//p.waitFor
	 	}
	 	
		
	 }
		
	
	/**
	 * TP 4 - Question 1
	 * Test le nombre de possibilit�
	 */
	def void testNbVariante(String nomVideogen){
		val videoGen = new VideoGenHelper().loadVideoGenerator(URI.createURI(nomVideogen + ".videogen"))
		assertNotNull(videoGen)
		
		// Variable calculant le nombre de variantes de vid�os
		val n = nbVariantes(videoGen)
		
		// Lecture de videos-size.csv
		val reader = new BufferedReader(new FileReader("videos-size.csv"));
		var lines = 0
		while (reader.readLine() != null) {
		    lines = lines + 1
		}
		reader.close()
		
		assertEquals(n, lines-1)
	}
	
	
	def int nbVariantes(VideoGeneratorModel videoGen){
		this.res = 0
		this.alternatives = 0
		
		videoGen.medias.forEach [video |	
			// Si c'est un mandatory
			if (video instanceof MandatoryVideoSeq ){
				if(this.res == 0){
					this.res = 1
				}
			}
			// Si c'est une vid�o alternative
			else if (video instanceof AlternativeVideoSeq ){
				this.alternatives += video.videodescs.size
			}
			// Si c'est une vid�o optionnelle
			else if (video instanceof OptionalVideoSeq ){
				if(this.res == 0){
					this.res = 2
				} else {
					this.res *= 2
				}
			}
		]
		
		// S'il n'y a que des vid�os alternatives
		if(this.res == 0 && this.alternatives != 0){
			this.res = 1
		}
		// S'il n'y a pas de vid�os alternatives
		if(this.res != 0 && this.alternatives == 0){
			this.alternatives = 1			
		}
		
		return this.res * this.alternatives;
	}
	
	
	/**
	 * TP3 - Question 1
	 */
	def void createCSV(String nomVideogen){
		val videoGen = new VideoGenHelper().loadVideoGenerator(URI.createURI(nomVideogen + ".videogen"))
		val videoGenHelper = new VideoGenHelper()
		
		assertNotNull(videoGen)
		println(videoGen.information.authorName)		
		
		generateCsv(videoGenHelper, nomVideogen)	
	}
	
	
	def ArrayList<ArrayList<VideoDescription>> listVariantVideos(VideoGeneratorModel videoGeneratorModel) {
        var res = new ArrayList<ArrayList<VideoDescription>>
        res.add(new ArrayList<VideoDescription>())
        
        for (video : videoGeneratorModel.medias) {
        	// Si c'est une alternative
            if (video instanceof AlternativeVideoSeq) {
                var listTmp = new ArrayList<ArrayList<VideoDescription>>
                for (vids : video.videodescs) {
                    for (list : res) {
                        var tmp = list.clone as ArrayList<VideoDescription>
                        tmp.add(vids)
                        listTmp.add(tmp)
                    }
                }
                res = listTmp
            } 
            // Si c'est une mandatory
            else if (video instanceof MandatoryVideoSeq) {
                
                for (list : res) {
                    list.add(video.description)
                }
            } 
            // Si c'est une optionnelle
            else if (video instanceof OptionalVideoSeq) {
                var listTmp = new ArrayList<ArrayList<VideoDescription>>
                for (list : res) {
                    var tmp = list.clone as ArrayList<VideoDescription>
                    listTmp.add(list)
                    tmp.add(video.description)
                    listTmp.add(tmp)
                }
                res = listTmp
            } 
        }
        return res
    }
	
	
	def void generateCsv(VideoGenHelper videoGenHelper, String nomVideogen) {
        var String content = "";
        var videogen = videoGenHelper.loadVideoGenerator(URI.createURI(nomVideogen + ".videogen"))
        var listVariantVid = new LinkedList<LinkedList<VideoDescription>>
        
        // Taille de chaque vid�o
        for (listVariantVideos : listVariantVideos(videogen)) {
            
            var temp = new LinkedList<VideoDescription>
            for (var int i = 0; i < listVariantVideos.size(); i++) {
                var VideoDescription currentVideo = listVariantVideos.get(i)
                temp.add(currentVideo)
            }
            listVariantVid.add(temp)
        }
        content += "realsize;id;"
        val videos = videogen
        var List<String> nameColumns = new LinkedList<String>()
        
        for (video : videos.medias) {
        	// Si c'est un mandatory
            if (video instanceof MandatoryVideoSeq) {
                nameColumns.add(video.description.videoid)
                content += video.description.videoid + ";"
            } 
            // Si c'est une vid�o optionnelle
            else if (video instanceof OptionalVideoSeq) {
                nameColumns.add(video.description.videoid)
                content += video.description.videoid + ";"
            }
            // Si c'est une vid�o alternative
            else if (video instanceof AlternativeVideoSeq) {
                for (videodesc : video.videodescs){
                    nameColumns.add(videodesc.videoid)
                    content += videodesc.videoid + ";"
                }
            }
        }
        
        content += "size\n"
        
        // Met les TRUE et FALSE dans le CSV
        
        var int ii = 1
        for (listVariantVideos : listVariantVid) {
            var int size = 0;
            content += ";" + (ii) + ";"
            var List<String> listName = new ArrayList<String>()
            for (var int i = 0; i < listVariantVideos.size(); i++) {
                val File currentFile = new File(listVariantVideos.get(i).location)
                val currentFileSize = currentFile.length() as int
                size += currentFileSize
                listName.add(listVariantVideos.get(i).videoid)
            }
            
            for (name : nameColumns) {
                if (listName.contains(name)) {
                    content += "TRUE;"
                } else {
                    content += "FALSE;"
                }
            }
            content += size + "\n"
            ii++
        }
        val file = new FileWriter("videos-size.csv")
        file.write(content)
        file.close()    
        println("content cvs : " + content)
    }
    
    def void addRealSize(String nomMp4){
    	println("== Ajout de RealSize ==")
  
    	println("Ouverture de videos-size.csv...")
    	val file = new FileWriter("videos-size.csv", true)
    	println("... videos-size.csv ouvert")
    	
    	println("Ouverture de " + nomMp4 + "...")
    	val mp4 = new File(nomMp4);
    	println("... " + nomMp4 + " ouvert")
    	
    	println("Ajout de la longueur r�elle du mp4 dans le csv...")
    	file.write(mp4.length + "");
    	println("... ajout effectu�")
    	
    	println("Fermeture du fichier csv...")
    	file.close();
    	println("... fichier ferm�")
    	
    	println("== Ajout de RealSize ==")
    }
	
	/**
	 * Question 3
	 */
	def void generateGif(String nomMp4, String nomGif){
		supprimerFichier(nomGif);
		
		var p = Runtime.runtime.exec("ffmpeg -i " + nomMp4 + " " + nomGif + " -hide_banner -vf scale=960:-1");
		p.waitFor
		
		val difference = tailleVideoEtGifDifferente(nomMp4, nomGif);
		
		System.out.println("La taille de la vid�o est " + difference + " � la taille du gif");
	}
	
	/**
	 * Compare la taille de la vid�o � la taille du gif
	 * @return	Inf�rieure, Egale, Sup�rieure
	 */
	def String tailleVideoEtGifDifferente(String nomMp4, String nomGif){
		val gif = new File(nomGif);
		val video = new File(nomMp4);
		
		var res = "";
		
		if(video.length > gif.length){
			res = "sup�rieure";
		} else if(video.length < gif.length){
			res = "inf�rieure";
		} else {
			res = "�gale";
		}
		
		return res;
	}
	
	/**
	 * Supprime le fichier s'il existe d�j�
	 */
	 def void supprimerFichier(String nomFichier){
	 	try{
	 		val f = new File(nomFichier);
	 		
	 		System.out.println("Suppression du fichier " + nomFichier);
	 		
	 		var f_supprime = f.delete()
	 		
	 		if(f_supprime){
	 			System.out.println("... Fichier " + nomFichier + " supprim�");
	 		} else {
	 			System.out.println("... erreur dans la suppression du fichier " + nomFichier);
	 		}
	 	} catch(Exception e){
	 		System.out.println("Le fichier " + nomFichier + " n'existe pas, pas besoin de le supprimer")
	 	}
	 }
	 
}