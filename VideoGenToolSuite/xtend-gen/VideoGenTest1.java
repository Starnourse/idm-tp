import com.google.common.base.Objects;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.function.Consumer;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.junit.Assert;
import org.junit.Test;
import org.xtext.example.mydsl.videoGen.AlternativeVideoSeq;
import org.xtext.example.mydsl.videoGen.BlackWhiteFilter;
import org.xtext.example.mydsl.videoGen.Filter;
import org.xtext.example.mydsl.videoGen.FlipFilter;
import org.xtext.example.mydsl.videoGen.MandatoryVideoSeq;
import org.xtext.example.mydsl.videoGen.Media;
import org.xtext.example.mydsl.videoGen.NegateFilter;
import org.xtext.example.mydsl.videoGen.OptionalVideoSeq;
import org.xtext.example.mydsl.videoGen.VideoDescription;
import org.xtext.example.mydsl.videoGen.VideoGeneratorModel;

@SuppressWarnings("all")
public class VideoGenTest1 {
  private int res = 0;
  
  private int alternatives = 0;
  
  private Object random = new Random();
  
  @Test
  public void testLoadModel() {
    try {
      final String nomVideogen = "example1";
      final String nomMp4 = "out1.mp4";
      final String nomGif = "out1.gif";
      final VideoGeneratorModel videoGen = new VideoGenHelper().loadVideoGenerator(URI.createURI((nomVideogen + ".videogen")));
      Assert.assertNotNull(videoGen);
      InputOutput.<String>println(videoGen.getInformation().getAuthorName());
      final FileWriter file = new FileWriter("playlistFile.txt");
      final Consumer<Media> _function = (Media video) -> {
        try {
          if ((video instanceof MandatoryVideoSeq)) {
            final VideoDescription desc = ((MandatoryVideoSeq)video).getDescription();
            String filtre = this.findFilter(desc.getFilter());
            String sousTitre = desc.getText().getContent();
            String _videoid = desc.getVideoid();
            String _plus = ("Adding mandatory video ..." + _videoid);
            InputOutput.<String>println(_plus);
            InputOutput.<String>println(desc.getLocation());
            String _content = desc.getText().getContent();
            boolean _equals = Objects.equal(_content, null);
            if (_equals) {
              sousTitre = "";
            }
            if ((Objects.equal(filtre, "") && Objects.equal(sousTitre, ""))) {
              String _location = desc.getLocation();
              String _plus_1 = ("file \'" + _location);
              String _plus_2 = (_plus_1 + "\' \n");
              file.write(_plus_2);
            } else {
              String _location_1 = desc.getLocation();
              String _plus_3 = ("file \'modify_" + _location_1);
              String _plus_4 = (_plus_3 + "\' \n");
              file.write(_plus_4);
              this.applyFilterSubtitle(filtre, desc.getText().getContent(), desc.getLocation());
            }
          }
          if ((video instanceof AlternativeVideoSeq)) {
            InputOutput.<String>println("Adding alternative video ...");
            final Random random = new Random();
            final VideoDescription desc_1 = ((AlternativeVideoSeq)video).getVideodescs().get(random.nextInt(((AlternativeVideoSeq)video).getVideodescs().size()));
            String filtre_1 = this.findFilter(desc_1.getFilter());
            String sousTitre_1 = desc_1.getText().getContent();
            String _content_1 = desc_1.getText().getContent();
            boolean _equals_1 = Objects.equal(_content_1, null);
            if (_equals_1) {
              sousTitre_1 = "";
            }
            if ((Objects.equal(filtre_1, "") && Objects.equal(sousTitre_1, ""))) {
              String _location_2 = desc_1.getLocation();
              String _plus_5 = ("file \'" + _location_2);
              String _plus_6 = (_plus_5 + "\' \n");
              file.write(_plus_6);
            } else {
              String _location_3 = desc_1.getLocation();
              String _plus_7 = ("file \'modify_" + _location_3);
              String _plus_8 = (_plus_7 + "\' \n");
              file.write(_plus_8);
              this.applyFilterSubtitle(filtre_1, sousTitre_1, desc_1.getLocation());
            }
          }
          if ((video instanceof OptionalVideoSeq)) {
            final Random random_1 = new Random();
            final VideoDescription desc_2 = ((OptionalVideoSeq)video).getDescription();
            String filtre_2 = this.findFilter(desc_2.getFilter());
            String sousTitre_2 = desc_2.getText().getContent();
            String probability = desc_2.getProbability();
            String _videoid_1 = desc_2.getVideoid();
            String _plus_9 = ("Adding optional video ..." + _videoid_1);
            InputOutput.<String>println(_plus_9);
            InputOutput.<String>println(desc_2.getLocation());
            String _content_2 = desc_2.getText().getContent();
            boolean _equals_2 = Objects.equal(_content_2, null);
            if (_equals_2) {
              sousTitre_2 = "";
            }
            String _probability = desc_2.getProbability();
            boolean _equals_3 = Objects.equal(_probability, "");
            if (_equals_3) {
              probability = "50";
            }
            if ((Objects.equal(filtre_2, "") && Objects.equal(sousTitre_2, ""))) {
              double _nextDouble = random_1.nextDouble();
              double _parseDouble = Double.parseDouble(probability);
              double _divide = (_parseDouble / 100);
              boolean _lessEqualsThan = (_nextDouble <= _divide);
              if (_lessEqualsThan) {
                String _location_4 = desc_2.getLocation();
                String _plus_10 = ("file \'" + _location_4);
                String _plus_11 = (_plus_10 + "\' \n");
                file.write(_plus_11);
              }
            } else {
              double _nextDouble_1 = random_1.nextDouble();
              double _parseDouble_1 = Double.parseDouble(probability);
              double _divide_1 = (_parseDouble_1 / 100);
              boolean _lessEqualsThan_1 = (_nextDouble_1 <= _divide_1);
              if (_lessEqualsThan_1) {
                String _location_5 = desc_2.getLocation();
                String _plus_12 = ("file \'modify_" + _location_5);
                String _plus_13 = (_plus_12 + "\' \n");
                file.write(_plus_13);
                this.applyFilterSubtitle(filtre_2, sousTitre_2, desc_2.getLocation());
              }
            }
          }
        } catch (Throwable _e) {
          throw Exceptions.sneakyThrow(_e);
        }
      };
      videoGen.getMedias().forEach(_function);
      file.close();
      InputOutput.<String>println(("ffmpeg -y -f concat -safe 0 -i playlistFile.txt -c copy " + nomMp4));
      Process p = Runtime.getRuntime().exec(("ffmpeg -y -f concat -safe 0 -i playlistFile.txt -c copy " + nomMp4));
      p.waitFor();
      Runtime.getRuntime().exec(("ffplay -autoexit " + nomMp4));
      InputOutput.<String>println("Cr�ation de la videoGen");
      this.createCSV(nomVideogen);
      this.testNbVariante(nomVideogen);
      this.generateGif(nomMp4, nomGif);
      this.addRealSize(nomMp4);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  public String findFilter(final Filter filter) {
    if ((filter instanceof BlackWhiteFilter)) {
      return "b&w";
    } else {
      if ((filter instanceof NegateFilter)) {
        return "negate";
      } else {
        if ((filter instanceof FlipFilter)) {
          return ((FlipFilter)filter).getOrientation();
        }
      }
    }
    return "";
  }
  
  /**
   * Applique un filtre et un sous-titre � la vid�o
   * TP4 - Q7
   */
  public void applyFilterSubtitle(final String filtre, final String drawtext, final String videoName) {
    try {
      InputOutput.<String>println(((((("Modify video : " + filtre) + " / ") + drawtext) + " / ") + videoName));
      boolean _equals = Objects.equal(filtre, "negate");
      if (_equals) {
        InputOutput.<String>println("== negate ==");
        Process p = Runtime.getRuntime().exec(((((("ffmpeg -y -i " + videoName) + " -vf lutrgb=\"r=negval:g=negval:b=negval, drawtext=fontfile=FreeSerif.ttf: text=\'") + drawtext) + "\'\" modify_") + videoName));
      } else {
        boolean _equals_1 = Objects.equal(filtre, "b&w");
        if (_equals_1) {
          InputOutput.<String>println("== b&w == ");
          Process p_1 = Runtime.getRuntime().exec(((((("ffmpeg -y -i " + videoName) + " -vf \"hue=s=0, drawtext=fontfile=FreeSerif.ttf: text=\'") + drawtext) + "\'\" modify_") + videoName));
        } else {
          if ((Objects.equal(filtre, "v") || Objects.equal(filtre, "vertical"))) {
            InputOutput.<String>println("== v == ");
            Process p_2 = Runtime.getRuntime().exec(((((("ffmpeg -y -i " + videoName) + " -vf \"transpose=1, drawtext=fontfile=FreeSerif.ttf: text=\'") + drawtext) + "\'\" modify_") + videoName));
          } else {
            if ((Objects.equal(filtre, "h") || Objects.equal(filtre, "horizontal"))) {
              InputOutput.<String>println("== h ==");
              Process p_3 = Runtime.getRuntime().exec(((((("ffmpeg -y -i " + videoName) + " -vf \"transpose=2,transpose=2, drawtext=fontfile=FreeSerif.ttf: text=\'") + drawtext) + "\'\" modify_") + videoName));
            } else {
              if ((Objects.equal(filtre, "") && (!Objects.equal(drawtext, "")))) {
                InputOutput.<String>println("== ?? ==");
                Process p_4 = Runtime.getRuntime().exec(((((("ffmpeg -y -i " + videoName) + " -vf \"drawtext=fontfile=FreeSerif.ttf: text=\'") + drawtext) + "\'\" modify_") + videoName));
              }
            }
          }
        }
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  /**
   * TP 4 - Question 1
   * Test le nombre de possibilit�
   */
  public void testNbVariante(final String nomVideogen) {
    try {
      final VideoGeneratorModel videoGen = new VideoGenHelper().loadVideoGenerator(URI.createURI((nomVideogen + ".videogen")));
      Assert.assertNotNull(videoGen);
      final int n = this.nbVariantes(videoGen);
      FileReader _fileReader = new FileReader("videos-size.csv");
      final BufferedReader reader = new BufferedReader(_fileReader);
      int lines = 0;
      while ((!Objects.equal(reader.readLine(), null))) {
        lines = (lines + 1);
      }
      reader.close();
      Assert.assertEquals(n, (lines - 1));
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  public int nbVariantes(final VideoGeneratorModel videoGen) {
    this.res = 0;
    this.alternatives = 0;
    final Consumer<Media> _function = (Media video) -> {
      if ((video instanceof MandatoryVideoSeq)) {
        if ((this.res == 0)) {
          this.res = 1;
        }
      } else {
        if ((video instanceof AlternativeVideoSeq)) {
          int _alternatives = this.alternatives;
          int _size = ((AlternativeVideoSeq)video).getVideodescs().size();
          this.alternatives = (_alternatives + _size);
        } else {
          if ((video instanceof OptionalVideoSeq)) {
            if ((this.res == 0)) {
              this.res = 2;
            } else {
              int _res = this.res;
              this.res = (_res * 2);
            }
          }
        }
      }
    };
    videoGen.getMedias().forEach(_function);
    if (((this.res == 0) && (this.alternatives != 0))) {
      this.res = 1;
    }
    if (((this.res != 0) && (this.alternatives == 0))) {
      this.alternatives = 1;
    }
    return (this.res * this.alternatives);
  }
  
  /**
   * TP3 - Question 1
   */
  public void createCSV(final String nomVideogen) {
    final VideoGeneratorModel videoGen = new VideoGenHelper().loadVideoGenerator(URI.createURI((nomVideogen + ".videogen")));
    final VideoGenHelper videoGenHelper = new VideoGenHelper();
    Assert.assertNotNull(videoGen);
    InputOutput.<String>println(videoGen.getInformation().getAuthorName());
    this.generateCsv(videoGenHelper, nomVideogen);
  }
  
  public ArrayList<ArrayList<VideoDescription>> listVariantVideos(final VideoGeneratorModel videoGeneratorModel) {
    ArrayList<ArrayList<VideoDescription>> res = new ArrayList<ArrayList<VideoDescription>>();
    ArrayList<VideoDescription> _arrayList = new ArrayList<VideoDescription>();
    res.add(_arrayList);
    EList<Media> _medias = videoGeneratorModel.getMedias();
    for (final Media video : _medias) {
      if ((video instanceof AlternativeVideoSeq)) {
        ArrayList<ArrayList<VideoDescription>> listTmp = new ArrayList<ArrayList<VideoDescription>>();
        EList<VideoDescription> _videodescs = ((AlternativeVideoSeq)video).getVideodescs();
        for (final VideoDescription vids : _videodescs) {
          for (final ArrayList<VideoDescription> list : res) {
            {
              Object _clone = list.clone();
              ArrayList<VideoDescription> tmp = ((ArrayList<VideoDescription>) _clone);
              tmp.add(vids);
              listTmp.add(tmp);
            }
          }
        }
        res = listTmp;
      } else {
        if ((video instanceof MandatoryVideoSeq)) {
          for (final ArrayList<VideoDescription> list_1 : res) {
            list_1.add(((MandatoryVideoSeq)video).getDescription());
          }
        } else {
          if ((video instanceof OptionalVideoSeq)) {
            ArrayList<ArrayList<VideoDescription>> listTmp_1 = new ArrayList<ArrayList<VideoDescription>>();
            for (final ArrayList<VideoDescription> list_2 : res) {
              {
                Object _clone = list_2.clone();
                ArrayList<VideoDescription> tmp = ((ArrayList<VideoDescription>) _clone);
                listTmp_1.add(list_2);
                tmp.add(((OptionalVideoSeq)video).getDescription());
                listTmp_1.add(tmp);
              }
            }
            res = listTmp_1;
          }
        }
      }
    }
    return res;
  }
  
  public void generateCsv(final VideoGenHelper videoGenHelper, final String nomVideogen) {
    try {
      String content = "";
      VideoGeneratorModel videogen = videoGenHelper.loadVideoGenerator(URI.createURI((nomVideogen + ".videogen")));
      LinkedList<LinkedList<VideoDescription>> listVariantVid = new LinkedList<LinkedList<VideoDescription>>();
      ArrayList<ArrayList<VideoDescription>> _listVariantVideos = this.listVariantVideos(videogen);
      for (final ArrayList<VideoDescription> listVariantVideos : _listVariantVideos) {
        {
          LinkedList<VideoDescription> temp = new LinkedList<VideoDescription>();
          for (int i = 0; (i < listVariantVideos.size()); i++) {
            {
              VideoDescription currentVideo = listVariantVideos.get(i);
              temp.add(currentVideo);
            }
          }
          listVariantVid.add(temp);
        }
      }
      String _content = content;
      content = (_content + "realsize;id;");
      final VideoGeneratorModel videos = videogen;
      List<String> nameColumns = new LinkedList<String>();
      EList<Media> _medias = videos.getMedias();
      for (final Media video : _medias) {
        if ((video instanceof MandatoryVideoSeq)) {
          nameColumns.add(((MandatoryVideoSeq)video).getDescription().getVideoid());
          String _content_1 = content;
          String _videoid = ((MandatoryVideoSeq)video).getDescription().getVideoid();
          String _plus = (_videoid + ";");
          content = (_content_1 + _plus);
        } else {
          if ((video instanceof OptionalVideoSeq)) {
            nameColumns.add(((OptionalVideoSeq)video).getDescription().getVideoid());
            String _content_2 = content;
            String _videoid_1 = ((OptionalVideoSeq)video).getDescription().getVideoid();
            String _plus_1 = (_videoid_1 + ";");
            content = (_content_2 + _plus_1);
          } else {
            if ((video instanceof AlternativeVideoSeq)) {
              EList<VideoDescription> _videodescs = ((AlternativeVideoSeq)video).getVideodescs();
              for (final VideoDescription videodesc : _videodescs) {
                {
                  nameColumns.add(videodesc.getVideoid());
                  String _content_3 = content;
                  String _videoid_2 = videodesc.getVideoid();
                  String _plus_2 = (_videoid_2 + ";");
                  content = (_content_3 + _plus_2);
                }
              }
            }
          }
        }
      }
      String _content_3 = content;
      content = (_content_3 + "size\n");
      int ii = 1;
      for (final LinkedList<VideoDescription> listVariantVideos_1 : listVariantVid) {
        {
          int size = 0;
          String _content_4 = content;
          content = (_content_4 + ((";" + Integer.valueOf(ii)) + ";"));
          List<String> listName = new ArrayList<String>();
          for (int i = 0; (i < listVariantVideos_1.size()); i++) {
            {
              String _location = listVariantVideos_1.get(i).getLocation();
              final File currentFile = new File(_location);
              long _length = currentFile.length();
              final int currentFileSize = ((int) _length);
              int _size = size;
              size = (_size + currentFileSize);
              listName.add(listVariantVideos_1.get(i).getVideoid());
            }
          }
          for (final String name : nameColumns) {
            boolean _contains = listName.contains(name);
            if (_contains) {
              String _content_5 = content;
              content = (_content_5 + "TRUE;");
            } else {
              String _content_6 = content;
              content = (_content_6 + "FALSE;");
            }
          }
          String _content_7 = content;
          String _plus_2 = (Integer.valueOf(size) + "\n");
          content = (_content_7 + _plus_2);
          ii++;
        }
      }
      final FileWriter file = new FileWriter("videos-size.csv");
      file.write(content);
      file.close();
      InputOutput.<String>println(("content cvs : " + content));
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  public void addRealSize(final String nomMp4) {
    try {
      InputOutput.<String>println("== Ajout de RealSize ==");
      InputOutput.<String>println("Ouverture de videos-size.csv...");
      final FileWriter file = new FileWriter("videos-size.csv", true);
      InputOutput.<String>println("... videos-size.csv ouvert");
      InputOutput.<String>println((("Ouverture de " + nomMp4) + "..."));
      final File mp4 = new File(nomMp4);
      InputOutput.<String>println((("... " + nomMp4) + " ouvert"));
      InputOutput.<String>println("Ajout de la longueur r�elle du mp4 dans le csv...");
      long _length = mp4.length();
      String _plus = (Long.valueOf(_length) + "");
      file.write(_plus);
      InputOutput.<String>println("... ajout effectu�");
      InputOutput.<String>println("Fermeture du fichier csv...");
      file.close();
      InputOutput.<String>println("... fichier ferm�");
      InputOutput.<String>println("== Ajout de RealSize ==");
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  /**
   * Question 3
   */
  public void generateGif(final String nomMp4, final String nomGif) {
    try {
      this.supprimerFichier(nomGif);
      Process p = Runtime.getRuntime().exec((((("ffmpeg -i " + nomMp4) + " ") + nomGif) + " -hide_banner -vf scale=960:-1"));
      p.waitFor();
      final String difference = this.tailleVideoEtGifDifferente(nomMp4, nomGif);
      System.out.println((("La taille de la vid�o est " + difference) + " � la taille du gif"));
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  /**
   * Compare la taille de la vid�o � la taille du gif
   * @return	Inf�rieure, Egale, Sup�rieure
   */
  public String tailleVideoEtGifDifferente(final String nomMp4, final String nomGif) {
    final File gif = new File(nomGif);
    final File video = new File(nomMp4);
    String res = "";
    long _length = video.length();
    long _length_1 = gif.length();
    boolean _greaterThan = (_length > _length_1);
    if (_greaterThan) {
      res = "sup�rieure";
    } else {
      long _length_2 = video.length();
      long _length_3 = gif.length();
      boolean _lessThan = (_length_2 < _length_3);
      if (_lessThan) {
        res = "inf�rieure";
      } else {
        res = "�gale";
      }
    }
    return res;
  }
  
  /**
   * Supprime le fichier s'il existe d�j�
   */
  public void supprimerFichier(final String nomFichier) {
    try {
      final File f = new File(nomFichier);
      System.out.println(("Suppression du fichier " + nomFichier));
      boolean f_supprime = f.delete();
      if (f_supprime) {
        System.out.println((("... Fichier " + nomFichier) + " supprim�"));
      } else {
        System.out.println(("... erreur dans la suppression du fichier " + nomFichier));
      }
    } catch (final Throwable _t) {
      if (_t instanceof Exception) {
        final Exception e = (Exception)_t;
        System.out.println((("Le fichier " + nomFichier) + " n\'existe pas, pas besoin de le supprimer"));
      } else {
        throw Exceptions.sneakyThrow(_t);
      }
    }
  }
}
